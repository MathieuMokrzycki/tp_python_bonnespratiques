# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 15:03:43 2020

@author: mathieu.mokrzycki
"""

import unittest
import logging

from Calculator.SimpleCalculator import SimpleCalculator

"""
on importe le package contenant notre classe pour pouvoir utiliser celle-ci
"""

############# Test functions ############

class AdditionTestSuite(unittest.TestCase):
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()

    def test_addition_two_integers(self):
        result = self.calculator.sum(5, 6)
        self.assertEqual(result, 11)
        logging.warning('test_addition_two_integer ok warn')  # will print a message to the console
        logging.info('test_addition_two_integer ok inf')  # will not print anything

    def test_addition_integer_string(self):
        result = self.calculator.sum(5, "6")
        self.assertEqual(result, "ERROR")
        logging.warning('test_addition_integer_string ERROR ok warn')  # will print a message to the console
        logging.info('test_addition_integer_string ERROT ok inf')  # will not print anything

    def test_addition_negative_integers(self):
        result = self.calculator.sum(-5, -6)
        self.assertEqual(result, -11)
        self.assertNotEqual(result, 11)
        logging.warning('test_addition_negative_integers ok warn')  # will print a message to the console
        logging.info('test_addition_negative_integers ok inf')  # will not print anything
        


class SubtractionTestSuite(unittest.TestCase):
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()

    def test_substration_two_integers(self):
        result = self.calculator.subtract(6, 5)
        self.assertEqual(result, 1)
        logging.warning('test_substration_two_integers ok warn')  # will print a message to the console
        logging.info('test_substration_two_integers ok inf')  # will not print anything

    def test_subtraction_integer_string(self):
        result = self.calculator.subtract(5, "6")
        self.assertEqual(result, "ERROR")
        logging.warning('test_subtraction_integer_string ERROR ok warn')  # will print a message to the console
        logging.info('test_subtraction_integer_string ERROT ok inf')  # will not print anything


    def test_subtraction_negative_integers(self):
        result = self.calculator.subtract(-5, -6)
        self.assertEqual(result, 1)
        self.assertNotEqual(result, -11) 
        logging.warning('test_subtraction_negative_integers ok warn')  # will print a message to the console
        logging.info('test_subtraction_negative_integers ok inf')  # will not print anything


class MultiplicationTestSuite(unittest.TestCase):
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()

    def test_multiplication_two_integers(self):
        result = self.calculator.multiply(6, 5)
        self.assertEqual(result, 30)
        logging.warning('test_multiplication_two_integers ok warn')  # will print a message to the console
        logging.info('test_multiplication_two_integers ok inf')  # will not print anything

    def test_multiplication_integer_string(self):
        result = self.calculator.subtract(5, "6")
        self.assertEqual(result, "ERROR")
        logging.warning('test_multiplication_integer_string ERROR ok warn')  # will print a message to the console
        logging.info('test_multiplication_integer_string ERROT ok inf')  # will not print anything

    def test_multiplication_negative_integers(self):
        result = self.calculator.multiply(-5, -6)
        self.assertEqual(result, 30)
        self.assertNotEqual(result, -30)  
        logging.warning('test_multiplication_negative_integers ok warn')  # will print a message to the console
        logging.info('test_multiplication_negative_integers ok inf')  # will not print anything


class DivisionTestSuite(unittest.TestCase):
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()

    def test_division_two_integers(self):
        result = self.calculator.divide(30, 5)
        self.assertEqual(result, 6)
        logging.warning('test_division_two_integers ok warn')  # will print a message to the console
        logging.info('test_division_two_integers ok inf')  # will not print anything

    def test_division_integer_string(self):
        result = self.calculator.divide(5, "6")
        self.assertEqual(result, "ERROR")
        logging.warning('test_division_integer_string ERROR ok warn')  # will print a message to the console
        logging.info('test_division_integer_string ERROT ok inf')  # will not print anything

    def test_division_negative_integers(self):
        result = self.calculator.divide(-30, -6)
        self.assertEqual(result, 5)
        self.assertNotEqual(result, -5)  
        logging.warning('test_division_negative_integers ok warn')  # will print a message to the console
        logging.info('test_division_negative_integers ok inf')  # will not print anything


    def test_divide_by_zero_exception(self):
        with self.assertRaises(ZeroDivisionError):
            self.calculator.divide(10, 0)
        logging.warning('test_divide_by_zero_exception ERROR ok warn')  # will print a message to the console
        logging.info('test_divide_by_zero_exception ERROR ok inf')  # will not print anything
            


if __name__ == "__main__":
    unittest.main()

    

################ Manual function tests ########################

#a = 7
#b = 0
#s = SimpleCalculator()
#print("We will test  5+7, 5-7, 5*7 et 5/7")
#print(s.sum(a, b))
#print(s.substract(a, b))
#print(s.multiply(a, b))
#
#try:
#    print(s.divide(a, b))
#
#except:
#    print("Cannot divide by zero")
