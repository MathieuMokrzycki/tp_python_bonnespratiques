# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 17:15:44 2020

@author: mathi
"""

from setuptools import setup

setup(
    name = 'TestSimpleCalculator2',
    version = '0.0.1',
    author = 'Mathieu Mokrzycki',
    packages = ['Calculator', 'Calculator/'],
    description = '''TestSimpleCalculator is a simple package 
    in order to make some test on packaging principles in Python''',
    license = 'GNU GPLv3',
    python_requires = '>=3.4',
)
