# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 14:04:13 2020

@author: mathieu.mokrzycki
"""

def somme(a,b):                 #definition of the sum fonction
    return a+b
    
def soustraction(a,b):          #definition of the soutraction fonction
    if a>b:
        return a-b
    else:
        return b-a
def multiplication(a,b):        #definition of the multiplication fonction
    return a*b

def division(a,b):              #definition of the division fonction
    if a>b:
      return a/b
    else:
      return b/a

#test of the different fonctions
#print(somme(1,5))
#print(soustraction(6,10))
#print(multiplication(2,7))
#print(division(15,6))


#I think to make a function to test if a and b are integers but it makes more lines in the code
#def ErrorInt(a,b):
#    if ((isinstance(a,int) == False) or (isinstance(a,int) == False)):
#        return "ERROR"
    

class SimpleCalculator:
   
    def sum(self, a, b):
        if ((isinstance(a,int) == False) or (isinstance(b,int) == False)):    #this line test if a and b are integers  
            return "ERROR"        
        else:
            return a + b

    def substract(self, a, b):
        if ((isinstance(a,int) == False) or (isinstance(b,int) == False)):    #this line test if a and b are integers  
            return "ERROR"        
        else:
            return a - b

    def multiply(self, a, b):
        if ((isinstance(a,int) == False) or (isinstance(b,int) == False)):    #this line test if a and b are integers  
            return "ERROR"        
        else:
            return a * b

    def divide(self, a, b):
        if ((isinstance(a,int) == False) or (isinstance(b,int) == False)):    #this line test if a and b are integers  
            return "ERROR"        
        else:
            return a / b


#I add the comment and adjust the indentation
