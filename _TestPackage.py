# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 15:03:43 2020

@author: mathieu.mokrzycki
"""

from Package_Python.BonnePratique import SimpleCalculator

"""
on importe le package contenant notre classe pour pouvoir utiliser celle-ci
"""

############# Test functions ############
a = 7
b = 0
s = SimpleCalculator()
print("We will test  5+7, 5-7, 5*7 et 5/7")
print(s.sum(a, b))
print(s.substract(a, b))
print(s.multiply(a, b))

try:
    print(s.divide(a, b))

except:
    print("Cannot divide by zero")